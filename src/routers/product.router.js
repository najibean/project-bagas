const { Router } = require('express');
const ProductController = require('../controllers/product.controller');
const responseHandler = require('../helpers/responseHandler');
const validate = require('./../validators/main.validator');
const productValidator = require('./../validators/product.validator');

const router = Router();

router.get('/test', (req, res, next) => {
  responseHandler.succes(res, 'Test route product');
});

router.get('/', ProductController.getAllProducts);
router.get('/:id', ProductController.getOneProduct);
router.post('/', validate(productValidator), ProductController.createProduct);
router.patch('/:id', ProductController.updateProduct);
router.delete('/:id', ProductController.deleteProduct);

module.exports = router;
